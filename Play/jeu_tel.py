#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 30 19:02:19 2021

@author: lamessen
"""

# Import des modules
import string

# Génération du dictionnaire (Clavier Tel, un seul appui par lettre)
lettres = string.ascii_lowercase
equiv = [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7,7,7,8,8,8,9,9,9,9]

trad = dict()
for elem in lettres:
    trad[elem] = equiv[ord(elem)-96-1]
    
##################################### Elements à tester
fic = open("/media/lamessen/Data/Python/divers/jeu_tel_parano/animateur_code", "r")
lignes = fic.readlines()
fic.close()
to_find = [] 
for ligne in lignes:
   to_find.append(ligne.strip().lower())

#################################### Lecture de la liste
# Fichier des animateurs, extrait Wikipédia
fic = open("/media/lamessen/Data/Python/divers/jeu_tel_parano/liste_animateur", "r")
lignes = fic.readlines()
fic.close()
animateurs = [] 
for ligne in lignes:
    animateurs.append(ligne.strip().lower())
    

#################################### Traduction des animateurs  
animateurs_code = list()
    
for nom in animateurs:
    nom_code = ''
    for letter in nom:
        if letter in lettres:
            nom_code += str(trad[letter])
        else:
            nom_code += letter
    animateurs_code.append(nom_code)
    
 #################################### Ecriture des identifiés       

nb_ok = 0
for ind in range(len(to_find)) :
    if to_find[ind] in animateurs_code:
        pos = animateurs_code.index(to_find[ind])
        print(f'{str(ind+1)}. {animateurs[pos]} ({to_find[ind]})')
        nb_ok +=1
    else:
        print(f'{str(ind+1)}. ???????????????????? ({to_find[ind]})')
        
        
print(f'Total : {nb_ok}/{len(to_find)}')

    
    
    
    
        