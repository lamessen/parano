#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Play with Parano
"""

# Imports 
from imdb import IMDb
from random import randint, shuffle
import re
import string
from tkinter import Tk
from tkinter.filedialog import askopenfilename

   
########################################
##    Nettoyage                       ##
########################################
def nettoyage(liste):   
    ''' nettoyage de la liste en entrée pour suppression des accents et caractères spéciaux'''
            
    liste_clean = []
    for elem in liste:
        elem = elem.strip()
        elem = re.sub('[ÉÈÊËéèêë]', 'e', elem)
        elem = re.sub('[ÀÂÄàâä]', 'a', elem)
        elem = re.sub('[ùÙ]', 'u', elem)
        elem = re.sub('[îiÎÏ]', 'i', elem)
        elem = re.sub('[øØ]', 'o', elem)
        elem = elem.lower()
        liste_clean.append(elem)
           
    return liste_clean
    
########################################
##    Génération des sources          ##
########################################
def liste_jeu(source,nombre):
    ''' Génère la liste en fonction du type sélectionné et du nombre de sorties voulue.
        source : 'animateur','serie','film'
        nombre : nbre de sorties à mettre
    '''
    
    ### mode présentateur
    if source == 'presentateur':
        with open('liste_animateur','r') as fic:
            lecture  = fic.readlines()
 
    ### mode fichier    
    elif source == 'fichier':
        try:
            Tk().withdraw()
            filename = askopenfilename()
            with open(filename,'r') as fic:
                lecture  = fic.readlines()
        except IOError:
            print("Le fichier indiqué n'est pas lisible")
            return
        
    ### mode manuel
    elif source == 'manuel':
        lecture =[]
        for i in range(int(nombre)):      
            lecture.append(input(f'Valeur n°{str(i+1)} : '))
            
    ### mode serie
    elif source == 'serie':
        lecture =[]        
        
        # Instanciation de IMDB
        db = IMDb()
        #lect_db = db.get_top250_tv() ## Ecarté car + difficile
        lect_db = db.get_popular100_tv()
        
        for elem in lect_db:
            lecture.append(elem['title'])
            
    ### Oups la source n'existe pas !
    else:
        print(f"liste_jeu : la source « {source} » n'existe pas")
        return
               
    # Limitation au nombre voulu
    liste = []
    for i in range(int(nombre)):
        liste.append(lecture.pop(randint(0,len(lecture)-1)))
            
    # Nettoyage de la liste
    liste = nettoyage(liste)
    
    # À vous les studios !    
    return liste
            
########################################
##          Generation des code       ##
########################################
def code_liste(liste,mode,aff=False):
    ''' Génère les éléments du jeu en fonction du mode sélectionné
        liste : la liste des propositions qui seront traduites
        mode  : type de jeu désiré :
                    - 'anagramme'  : anagramme des noms
                    - 'tel_unique' : quelle touche de téléphone correspond
                    - 'tel_full'   : quelle touche du tel, avec nombre d'appui
        aff : affichage des éléments générés
    '''

    # Initialisation
    coded = []
    
    ### mode anagramme
    if mode == 'anagramme':
        for elem in liste:
            
            # Espace maintenus à leurs places
            elem_splited = elem.split()
            elem_assembl = []
            
            for part in elem_splited:
                melange = list(part.strip())
                shuffle(melange)
                elem_assembl.append(''.join(melange))

            coded.append(' '.join(elem_assembl))
        
    ### mode tel unique
    elif mode == 'tel_unique':
        # Génération du dictionnaire (Clavier Tel, un seul appui par lettre)
        lettres = string.ascii_lowercase
        equiv = [2,2,2,3,3,3,4,4,4,5,5,5,6,6,6,7,7,7,7,8,8,8,9,9,9,9]
        
        trad = dict()
        for elem in lettres:
            trad[elem] = equiv[ord(elem)-96-1]
                    
        # Traduction
        for nom in liste:
            nom_coded = ''
            for letter in nom:
                if letter in lettres:
                    nom_coded += str(trad[letter])
                else:
                    nom_coded += letter
            coded.append(nom_coded)
    
     ### mode tel full  
    elif mode =='tel_full':    
        # Génération du dictionnaire (Clavier Tel, un seul appui par lettre)
        lettres = string.ascii_lowercase
        equiv = [2,22,222,3,33,333,4,44,444,5,55,555,6,66,666,7,77,777,7777,8,88,888,9,99,999,9999]
        
        trad = dict()
        for elem in lettres:
            trad[elem] = equiv[ord(elem)-96-1]
                    
        # Traduction
        for nom in liste:
            nom_coded = ''
            for letter in nom:
                if letter in lettres:
                    nom_coded += str(trad[letter])
                else:
                    nom_coded += letter
            coded.append(nom_coded)
    
    
     ### Oups le mode n'existe pas ! 
    else:
        print(f"genere_jeu : le mode « {mode} » n'existe pas")
        return   
        
    # Affichage si demandé, sinon sortie de la fonction
    if aff:
        print('#'*50)
        print(f'# Jeu    : {mode}')
        print(f'# Nombre : {len(liste)}')
        print('#'*50)
        for ind in range(len(liste)) :
            print(f'{str(ind+1)}. {coded[ind]} ({liste[ind]})')
        print('#'*50) 
    else:       
        # retour pour décodage
        return coded
    