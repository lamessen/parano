<?php
	// POINT 1)
	// ACCREDITATION
	if ( !isset($_POST['post_accred']) )
	{
		$accreditation = "orange";
	}
	
	if ( isset($_POST['post_accred']) )
	{
		switch ($_POST['post_accred'])
		{
			case "ir":
				$accreditation = "infra-rouge";
			break;
			
			case "r":
				$accreditation = "rouge";
			break;
			
			case "o":
				$accreditation = "orange";
			break;
			
			case "j":
				$accreditation = "jaune";
			break;
			
			case "v":
				$accreditation = "vert";
			break;
			
			case "b":
				$accreditation = "bleu";
			break;
			
			case "vi":
				$accreditation = "violet";
			break;
			
			case "i":
				$accreditation = "indigo";
			break;
			
			case "uv":
				$accreditation = "ultra-violet";
			break;
			
			default:
				$accreditation = "orange";
		}
	}
	
	// THEMES	
	if ( !isset($_POST['post_theme']) )
	{
		$theme_base = "default/base";
	}
	
	if ( isset($_POST['post_theme']) )
	{
		switch ($_POST['post_theme'])
		{
			case "euroot":
				$theme_base = "euroot";
			break;
			
			case "hyppolite":
				$theme_base = "hyppolite";
			break;
			
			case "kaeme":
				$theme_base = "kaeme";
			break;
			
			case "kaeme-light":
				$theme_base = "kaeme-light";
			break;
			
			case "neon":
				$theme_base = "neon";
			break;
			
			case "nwk":
				$theme_base = "nwk";
			break;
			
			case "opale-light":
				$theme_base = "opale-light";
			break;
			
			case "opale-full":
				$theme_base = "opale-full";
			break;
			
			case "opale-pink31":
				$theme_base = "opale-pink31";
			break;
			
			case "80vsOpale":
				$theme_base = "80vsOpale";
			break;
			
			case "monochrome":
				$theme_base = "monochrome";
			break;
			
			case "pinchou-light":
				$theme_base = "pinchou-light";
			break;
			
			default:
				$theme_base = "default/base";
		}
	}
?>

<?php
	// POINT 2)
	if ( !isset($_POST['post_titre']) )
	{
		$title_parser = NULL;
	}
	
	if ( isset($_POST['post_titre']) )
	{
		if ( $_POST['post_titre'] == NULL )
		{
			$title_parser = "[annonce] Votre propagande ici ! - ";
		}
		
		else
		{
			$title_parser = "" .stripslashes($_POST['post_titre']). " - ";
		}
	}
?>

<!DOCTYPE html>
<html dir="ltr" lang="fr">
<head>
	<meta charset="utf-8" />
	<title><?php echo $title_parser; ?>Parser paranoïaque</title>
	
	<link href="./CSS/base.css" rel="stylesheet" type="text/css" />
	<link href="./CSS/base-<?php echo $accreditation; ?>.css" rel="stylesheet" type="text/css" />
	
	<link href="./CSS/<?php echo $theme_base; ?>/<?php echo $theme_base; ?>.css" rel="stylesheet" type="text/css" />
	<link href="./CSS/<?php echo $theme_base; ?>/<?php echo $theme_base; ?>-<?php echo $accreditation; ?>.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript">
		// POINT 3)
		function main()
		{
			var post_accred = "<?php echo $accreditation; ?>";
			document.getElementById(post_accred).selected = true;
			
			var post_theme = "<?php echo $theme_base; ?>";
			document.getElementById(post_theme).selected = true;
		}
	</script>
</head>

<body onLoad="main();">
<div class="box_logo">
	<table border="0">
		<tr>
			<td width="100" ><a href="index.php" ><img src="./CSS/paranoiaque2009.gif" alt="www.parano.be" border="0"  /></a></td>
			<td width="100%">
				<div class="lordi">
					<small> Si vous lisez ceci, c'est que ça faisait longtemps que j'ai plus mis à jour le site. Bienvenue en 2015, Marty ! </small>
					<br />
					<small>[<a target="_blank" href="http://www.parano.be/v15/view.php?id=121605">SmokyBird-121605</a>]</small>
				</div>
			</td>
		</tr>
	</table>
</div>

<div class="box_menu"> 
	<span class="class_menu">
		<a class="bbs_menu" href="index.php">Parser paranoïaque</a> |
		<a class="bbs_menu" href="decoup.php">Découp'Galerie</a>
	</span>
</div>
 
<div class="box_fiche">
	<?php
		// POINT 4)
		if ( isset($_POST['post_propa']) )
		{
			// PROTECTIONS
			$propagande = stripslashes($_POST['post_propa']);
			$propagande = htmlspecialchars($propagande);
			$propagande = nl2br($propagande);
			
			$title = stripslashes($_POST['post_titre']);
			$title = htmlspecialchars($title);
			$title = nl2br($title);
		}
	?>
	<form  method="post" action="?post">
		<table border="0" cellpadding="10">
			<tr valign="top" >
			<td>
				<table border="0" cellpadding="5">				
					<tr valign="top">
						<td>Thème : </td>
						<td>
							<select name="post_theme">
								<option value="default" id="default/base">Parano (Theme 2003)</option>
								<option value="euroot" id="euroot">EuRooT Office</option>
								<option value="hyppolite" id="hyppolite" >Hyppolite </option>
								<option value="kaeme" id="kaeme">Kaeme (Thème arrondi)</option>
								<option value="kaeme-light" id="kaeme-light">Kaeme Light (Thème arrondi)</option>
								<option value="neon" id="neon">Neon Flashy (by  Kiloloan)</option>
								<option value="nwk" id="nwk">Nwk (BoitesArrondies)</option>
								<option value="opale-light" id="opale-light">Opale (Light)</option>
								<option value="opale-full" id="opale-full">Opale (My Rainbow Poney Style)</option>
								<option value="opale-pink31" id="opale-pink31">Opale (Spécial Pink31)</option>
								<option value="80vsOpale" id="80vsOpale">Opale versus 80_ (the Compromise)</option>
								<option value="monochrome" id="monochrome">Parano (Monochrome)</option>
								<option value="pinchou-light" id="pinchou-light">Pinchou (The Compromise Light)</option>								
							</select>
						</td>
					</tr>
					
					<tr valign="top">
						<td>Accréditation : </td>
						<td>
							<select name="post_accred">
								<option value="ir" id="infra-rouge">Infra-rouge</option>
								<option value="r" id="rouge">Rouge</option>
								<option value="o" id="orange">Orange</option>
								<option value="j" id="jaune">Jaune</option>
								<option value="v" id="vert">Vert</option>
								<option value="b" id="bleu">Bleu</option>
								<option value="vi" id="violet">Violet</option>
								<option value="i" id="indigo">Indigo</option>
								<option value="uv" id="ultra-violet">Ultra-Violet</option>
							</select>
						</td>
					</tr>
					
					<tr valign="top">
						<td>Titre : </td>
						<td><input name="post_titre" value="<?php if ( isset($_POST['post_titre']) ) { if ( $_POST['post_titre'] == NULL ) { echo "[interne / annonce] Votre propagande ici !"; } else { echo stripslashes($_POST['post_titre']); } } ?>" type="text" size="65" maxlength="65"></td>
					</tr>

					<tr valign="top" >
						<td>Propagande :</td>
						<td>
							<textarea name="post_propa" cols="65" rows="25" ><?php if ( isset($_POST['post_propa']) ) { echo stripslashes($_POST['post_propa']); } ?></textarea>
						</td>
					</tr>
					
					<tr>
						<td>
							<input class="button" type="submit" value="Proposer">
						</td>
					</tr>
				</table>
			</td>
				
			<td width="500">
				<div class="title_item">Ligne Editoriale</div>
				<p>MESSAGE IMPORTANT !</p>
				<p>Ce site ressemble à parano.be, il a la même fonctionnalité que parano.be, mais en AUCUN CAS, il ne s'agit de parano.be en lui-même. Sans mentir.</p>
				<p>Ce parseur a été développé par <a target="_blank" href="http://www.parano.be/v15/view.php?id=121605">SmokyBird-121605</a> afin de vous aider dans la mise en forme de votre propagande.</p>
				<p>Concernant son utilisation, c'est hyper-simple. Vous copiez-collez votre propagande, vous cliquez sur le bouton et vous observez le résultat en-dessous. Chouette hein ? En plus, vous pouvez personnaliser l'affichage de la propagande pour voir si c'est beau !</p>
				
				<p><b>Quelques astuces pour la mise en forme de votre propagande :</b><br />
				- *texte* : <i>texte en italique</i><br />
				- **texte** : <b>texte en gras</b><br />
				- ***texte*** : <b><i>texte en gras italique</i></b><br />
				<br />
				- http://lien.com : place le lien direct et cliquable<br />
				- http://image.jpg : affiche l'image directement sur la propa<br />
				- [texte](http://lien.com) : met "texte" cliquable qui pointera vers le lien<br />
				<br />
				- `texte` : affichera votre texte en code source
				<br />
				<br />
				<br />
				N'hésitez pas à partager ce site avec votre secteur, voire même dans votre ligne éditoriale. C'est toujours un plus pour les nouveaux (et les anciens) citoyens !</p>
			</td>
			</tr>
		</table>
	</form>

<?php
	if ( isset($_POST['post_propa']) )
	{
		// MISE EN FORME : ETOILES
		$propagande = preg_replace('#\*\*\*(.+)\*\*\*#isU', '<strong><em>$1</em></strong>', $propagande); // gras italique
		$propagande = preg_replace('#\*\*(.+)\*\*#isU', '<strong>$1</strong>', $propagande); // gras
		$propagande = preg_replace('#\*(.+)\*#isU', '<em>$1</em>', $propagande); // italique
		

		// MISE EN FORME : LIENS
		$propagande = preg_replace('#\[(.+)\]\((http[s]?://[a-zA-Z0-9._/;?=&\#-]*)\)#iU', '<a target="_blank" href="$2">$1</a>', $propagande); // lien + texte 
		$propagande = preg_replace('#(?<!href=")(http[s]?://[a-zA-Z0-9._/;?=&\#-]*)\.(jpe?g|png|gif)+#i', '<img src="$0" />', $propagande); // image 
		$propagande = preg_replace('#(?<!(?:href| src)=")(http[s]?://[a-zA-Z0-9._/;?=&\#-]*)#i', '<a target="_blank" href="$0">$0</a>', $propagande); // lien  


		// MISE EN FORME + CORRECTION : CODE
		$propagande = preg_replace('#\`\*\*\*(.+)\*\*\*\`#isU', '$1', $propagande); // code + gras italique
		$propagande = preg_replace('#\`\*\*(.+)\*\*\`#isU', '$1', $propagande); // code + gras
		$propagande = preg_replace('#\`\*(.+)\*\`#isU', '$1', $propagande); // code + italique
		$propagande = preg_replace('#\`(.+)\`#isU', '<code>$1</code>', $propagande); // code

		// CORRECTION : LIEN
		$propagande = preg_replace('#<a target="_blank" href="<a target="_blank" href="#', '<a target="_blank" href="', $propagande); // Souci début lien
		$propagande = preg_replace('#(http[s]?://[a-zA-Z0-9._/;?=&\#-]*)</a>">#', '</a>', $propagande); // Souci fin lien


		// CORRECTION : IMAGE
		$propagande = preg_replace('#<a href="<img src="#', '<img src="', $propagande); // Souci début image
		$propagande = preg_replace('#"><img src="(http[s]?://[a-zA-Z0-9._/;?=&\#-]*)" /></a>#', '', $propagande); // Souci fin image


		// CORRECTION : BUG LIENS
		$propagande = preg_replace('#\_#i', '_', $propagande);
		?>
			
			<hr />
			
			<div class="message-<?php echo $accreditation; ?>">
				<table border="0" cellpadding="4" class=" width: 100%; ">
					<tr valign="top">
						<td>
							<img src="./CSS/4e615514baa71_tiny.jpg" alt="image">
						</td>
						
						<td>
							<b><?php if ( $_POST['post_titre'] == NULL ) { echo "[interne / annonce] Votre propagande ici !"; } else { echo htmlspecialchars(stripslashes($_POST['post_titre'])); } ?></b>
							<br />
							
							<small>
								<i>Proposé par vous le <?php echo date("d"); ?>/<?php echo date("m"); ?>/<?php echo date("Y"); ?> ,Validé par <a href="http://www.parano.be/v15/view.php?id=121605">121605</a></i>
							</small>

							<p>
								<br />
									<?php echo $propagande; ?>
								<br />
							</p>
							<br />
							
							<p>
								[Lire la suite]
							</p>
						</td>
					</tr>
				</table>
			</div>
			
		<?php
	}
?>

</div>

<div class="copy">Ce message n'existe pas. Circulez, citoyens - Last update : 16/09/2015<br />
À tout les paranoïaques fans du service : n'hésitez pas à en parler dans votre ligne éditoriale / webzine / entourage ! Et un petit message de remerciements ne serait pas de trop :)</div>

</body>
</html>