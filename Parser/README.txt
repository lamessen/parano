Bienvenue, cher visiteur. Ou visiteuse.

Si vous �tes actuellement en train de lire le code source du site, f�licitations. vous allez vous retrouver sans doute devant un joyeux bordel incompr�hensible et sans doute illisible � vos yeux. Je suis d�sol� si vos yeux souffrent par la suite.

Le code ayant �t� fait en 2012, je dois avouer que certaines informations ne soient pas totalement logiques, car j'apprenais encore le PHP de mani�re ind�pendante. Donc il se peut parfaitement que des erreurs de d�butant se soient malencontreusement gliss�es dedans. Mais �a, c'�tait avant, vu que maintenant, j'ai acquis bien plus de connaissances en PHP, ainsi que en MySQL et en Javascript. Bref.

Tout �a pour dire que ci-dessous, vous trouverez le processus de l'outil par �tape, afin de vous simplifier la vie lors de la lecture. Ce qui est tr�s utile, et qui m�rite sans doute un bisous sur ma fiche. C'est facultatif, bien entendu.

En tout cas, je vous souhaite une bonne lecture du code et du processus, si besoin. Merci d'avance si cet outil se retrouve dans le BOTserv, c'est un plaisir de pouvoir contribuer au site, et ce pendant encore longtemps, tant que j'en aurai la motivation. Par ailleurs, si possible, j'aimerais avoir mon pseudo et/ou ID affich� sur l'outil, afin qu'on ne me vole pas l'id�e sauvagement (exemple : parano-parser.tk *tousse*).



SmokyBird-Vi-EDUC 121605 16 Septembre 2015 - 10h02


--------------------------------------------------

Processus par �tape (Parser) :
1) On r�cup�re d'abord l'accr�ditation et le th�me. S'il n'y a pas de $_POST, on prend par d�faut le th�me par d�faut et l'accr�ditation Orange.

2) Le code Javascript va "mettre" en m�moire les infos r�cup�r�es concernant le point pr�c�dent (avant, tout se faisait en PHP, et �a buggait de partout).

3) On met � jour le titre du site, si une propa a �t� envoy�e. Logique. Le point 2) va d'ailleurs fonctionner aussi � ce moment-l�, merci Javascript !

4) Si une propa a �t� envoy�e, on place des protections visuelles. Pourquoi protections ? Parce que si un retour � la ligne ne s'applique pas correctement, des slashs ou des caract�res sp�ciaux s'affichent, �a fait saigner les yeux. Parfois. (aussi, s�curit� de base quand on travaille avec des donn�es envoy�es vers un serveur)

5) On met en places la mise en forme de la propa selon le code. Si un DEV lit actuellement ceci, il se peut qu'il manque l'une ou l'autre mise en forme. Je me suis juste bas� sur des tests de propas en interne pour pouvoir cr�er ces mises en forme qui sont en g�n�ral utilis�es par 99% des citoyens.

6) On affiche la propagande mise en forme avec l'accr�ditation et le th�me choisi dans les listes. Tadaaaaaaa !


--------------------------------------------------

Processus par �tape (Decoup') :
1) On v�rifie qu'un fichier a �t� envoy�. Logique.

2) Si un fichier existe, on v�rifie son extension. Je n'ai accept� que les PNG, JPG et JPEG, qui sont des images tr�s souvent utilis�es sur le net, et ayant un bon rapport qualit�/poids (parce qu'un BMP, quand m�me, �a p�se lourd)/

3) Si l'extension est correct, on v�rifie la hauteur de l'image. D'apr�s mes souvenirs, la hauteur de l'image sur parano.be ne peut pas d�passer les 600 pixels, donc on fait pareil. Et j'ai �galement inclus le fait que si la hauteur est �gale � 0, on refuse le fichier (parce que �a n'est pas du tout une image).

4) Une fois les 3 points pr�c�dents v�rifi�, on va cr�er un dossier "images" pour stocker les images coup�s en 4 (mais �a, �a va venir plus tard). Chaque image sera dans un dossier portant un ID entre 1.000.000 et 9.999.999, et divis� en 4 fichiers distincts, sous le format "ID-x.png".

5) Une fois les dossiers pr�par�s, on va "cr�er" l'image histoire de la stocker. Il y a deux fois la m�me fonction "imagecreatefrom...", chacun r�serv� par rapport � l'extension (merci PHP...). On cr�e ensuite une variable qui retient en m�moire la taille de l'image, qui sera ici de 150px de large, et une hauteur �gale � l'image envoy�.

6) On entre dans une boucle pour cr�er les 4 images � partir de celle de base. Une incr�mentation est effectu�e pour se "d�placer" dans l'image sur la longeur, et on affiche l'image final.