<!DOCTYPE html>
<html dir="ltr" lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Découp'Galerie</title>
	
	<link href="./CSS/base.css" rel="stylesheet" type="text/css" />
	<link href=./CSS/base-orange.css" rel="stylesheet" type="text/css" />
	
	<script type="text/javascript">
		// POINT 3)
		function main()
		{
			document.getElementById("show_result").cellPadding = document.getElementsByName("cellpading")[0].value;
		}
	</script>
</head>

<body>
<div class="box_logo">
	<table border="0">
		<tr>
			<td width="100" ><a href="index.php" ><img src="./CSS/paranoiaque2009.gif" alt="www.parano.be" border="0"  /></a></td>
			<td width="100%">
				<div class="lordi">
					<small> Si vous lisez ceci, c'est que ça faisait 3 ans que j'ai plus mis à jour le site. Bienvenue en 2015, Marty ! </small>
					<br />
					<small>[<a target="_blank" href="http://www.parano.be/v15/view.php?id=121605">SmokyBird-121605</a>]</small>
				</div>
			</td>
		</tr>
	</table>
</div>

<div class="box_menu"> 
	<span class="class_menu">
		<a class="bbs_menu" href="index.php">Parser paranoïaque</a> |
		<a class="bbs_menu" href="decoup.php">Découp'Galerie</a>
	</span>
</div>

<div class="box_fiche">
		<div class="message_systeme">
			<center>Ce service permet de couper vos images en 4, afin de les placer directement dans votre galerie sans trop d'efforts.<br /><br />
			ATTENTION : meilleurs résultats attendus avec des images de 600px de large !<br /><br />
			Choisissez une image de votre disque dur (taille maximum : 600px de hauteur)</center>
		</div>
		
		<center>
			<form action="?post" enctype="multipart/form-data" method="POST">
				<input type="hidden" name="upload" />
				<input type="file" name="decoup-image" />

				<p>
					<input type="submit" value="  Uploader   ">
				</p>
			</form>
		</center>
		
		<?php
			// Si un $_POST existe, on exécute le code
			if ( isset($_FILES['decoup-image']) )
			{
				// Si rien n'a été envoyé (aucun fichier sélectionné)
				if ( $_FILES['decoup-image']['error'] == 4 )
				{
					?>
						
						<div class="message_systeme">
							<p><b>ERREUR !</b></p>
							<p>Aucune image n'a été uploadé...</p>
						</div>
						
					<?php
				}
				
				// Si un fichier a été sélectionné et envoyé
				if ( $_FILES['decoup-image']['error'] == 0 )
				{
					$file_info = pathinfo($_FILES['decoup-image']['name']);
					$check_extension = $file_info['extension'];
					$check_extension_array = array('png', 'jpeg', 'jpg');
					
					// Si le fichier ne possède pas l'extension requis ci-dessus
					if ( !in_array($check_extension, $check_extension_array) )
					{
						?>
							<div class="message_systeme">
								<p><b>ERREUR !</b></p>
								<p>Ce fichier n'est pas une image JP(E)G ou PNG...</p>
							</div>
							
						<?php
					}
					
					// Si le fichier possède l'extension requis
					if ( in_array($check_extension, $check_extension_array) )
					{
						$screen_size = getimagesize($_FILES['decoup-image']['tmp_name']);
						
						// Si la hauteur de l'image égale 0px (pas une véritable image) ou dépasse les 600px (image trop haute)
						if ( ($screen_size[1] == 0) OR ($screen_size[1] > 600) )
						{
							?>
						
								<div class="message_systeme">
									<p><b>ERREUR !</b></p>
									<p>L'image dépasse les 600px de hauteur (ou alors, c'est pas une image valide)</p>
								</div>
								
							<?php
						}
						
						else
						{
							$init_width = 0;
							$array_image = 0;
							
							$rand_num = mt_rand(1000000,9999999);
							mkdir('images/' .$rand_num. '/', 0711);
							
							if ( ($check_extension == "jpeg") OR ($check_extension == "jpg") )
							{
								$preparing_image = imagecreatefromjpeg($_FILES['decoup-image']['tmp_name']);
							}
							
							if ( ($check_extension == "png") )
							{
								$preparing_image = imagecreatefrompng($_FILES['decoup-image']['tmp_name']);
							}
							
							$crop_image_dest = imagecreatetruecolor(150, $screen_size[1]);
							
							?>
							
							<center>
								<p>Espacement des images (visuel uniquement) : </p>
								<select onChange="main();" onKeyUp="main();" name="cellpading">
									<option value="0">0</option>
									<option value="1">1</option>
									<option value="5">5</option>
									<option value="10" selected>10</option>
									<option value="15">15</option>
								</select>
							</center>
							
							<hr />
							<h3 style="text-align : center;">Et voilàààààààààà !</h3>
							
							<table id="show_result" cellpadding="10" border="0" cellspacing="0" align="center">
								<tr>
								<?php
									for ( $boucle_init = 4; $boucle_init > 0; $boucle_init-- )
									{		
										while ( $array_image < 4 )
										{
											imagecopy($crop_image_dest, $preparing_image, 0, 0, $init_width, 0, 150, $screen_size[1]);
											$init_width = $init_width + 150;
											$array_image++;
											
											if ( ($check_extension == "jpeg") OR ($check_extension == "jpg") )
											{
												imagepng($crop_image_dest, "images/" .$rand_num. "/" .$rand_num. "-" .$array_image. ".png");
											}
											
											if ( ($check_extension == "png") )
											{
												imagejpeg($crop_image_dest, "images/" .$rand_num. "/" .$rand_num. "-" .$array_image. ".png");
											}
											
											?>
												
												<td valign="top"><img src="images/<?php echo $rand_num; ?>/<?php echo $rand_num; ?>-<?php echo $array_image;?>.png" border="0" /></td>
												
											<?php
										}
									}
								?>
								</tr>
							</table>
						<?php
						}
					}
				}
			}
		?>
</div>

<div class="copy">Ce message n'existe pas. Circulez, citoyens - Last update : 16/09/2015<br />
À tout les paranoïaques fans du service : n'hésitez pas à en parler dans votre ligne éditoriale / webzine / entourage ! Et un petit message de remerciements ne serait pas de trop :)</div>

</body>
</html>